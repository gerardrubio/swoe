<?php

error_reporting(E_ALL);

/**
 * Read auto-loader
 */
include __DIR__ . '/../vendor/autoload.php';

use Phalcon\Mvc\Application;

$debug = new \Phalcon\Debug();
$debug->listen(true, true);

/**
 * Read the configuration
 */
$config = include __DIR__ . "/../app/config/config.php";

/**
 * Read services
 */
include __DIR__ . "/../vendor/mpwar/swoe/src/config/services.php";

/**
 * Handle the request
 */
$application = new Application($di);

echo $application->handle()->getContent();
