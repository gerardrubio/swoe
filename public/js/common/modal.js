/**
 * Created by Gerard on 22/08/2015.
 */
window.swoe = window.swoe || {};
window.swoe = (function (S, $) {

    S.Modal = (function() {
        var _defaults = {
            confirm: {
                approve: {
                    callback: function () {
                        return true;
                    },
                    color: 'green',
                    icon: 'checkmark',
                    text: 'Yes'
                },
                closeable: true,
                content: '',
                deny: {
                    callback: function () {
                        return true;
                    },
                    color: 'red',
                    icon: 'remove',
                    text: 'No'
                },
                header: {
                    color: '',
                    icon: '',
                    text: ''
                },
                template: '<div class="ui small basic modal"><h2 class="ui center aligned icon header"><i class="{header.icon} {header.color} icon"></i>{header.text}</h2><div class="content">{content}</div><div class="actions"><div class="ui {deny.color} basic cancel inverted button"><i class="{deny.icon} icon"></i>{deny.text}</div><div class="ui {approve.color} ok inverted button"><i class="{approve.icon} icon"></i>{approve.text}</div></div></div>'
            },
            message: {
                approve: {
                    callback: function () {
                        return true;
                    },
                    color: '',
                    icon: 'chevron right',
                    text: 'Continue'
                },
                closeable: true,
                content: '',
                deny: {
                    callback: function () {
                        return true;
                    },
                    color: 'red',
                    icon: 'remove',
                    text: 'No'
                },
                header: {
                    color: '',
                    icon: '',
                    text: ''
                },
                template: '<div class="ui small basic modal"><h2 class="ui center aligned icon header"><i class="{header.icon} {header.color} icon"></i>{header.text}</h2><div class="content">{content}</div><div class="actions"><div class="ui {approve.color} ok basic inverted button">{approve.text}<i class="{approve.icon} icon"></i></div></div></div>'
            }
        };

        var _parseTemplate = function(options) {
            return options.template
                .replace('{approve.color}', options.approve.color)
                .replace('{approve.icon}', options.approve.icon)
                .replace('{approve.text}', options.approve.text)
                .replace('{content}', options.content)
                .replace('{deny.color}', options.deny.color)
                .replace('{deny.icon}', options.deny.icon)
                .replace('{deny.text}', options.deny.text)
                .replace('{header.color}', options.header.color)
                .replace('{header.icon}', options.header.icon)
                .replace('{header.text}', options.header.text);
        };

        var _new = function (kind, options) {
            if (!_defaults.hasOwnProperty(kind))
                throw new Error('Invalid template');

            options = $.extend(true, _defaults[kind], options);

            var parsedTemplate = _parseTemplate(options);

            var modal = $(parsedTemplate);
            modal.modal({
                closable: options.closable,
                onApprove: options.approve.callback,
                onDeny: options.deny.callback
            }).modal('show');

            return modal;
        };

        return {
            show: _new
        };
    })();

    return S;
})(window.swoe || {}, jQuery);
