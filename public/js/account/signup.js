/**
 * Created by Gerard on 11/08/2015.
 */
(function($) {
    var form = $('.ui.form');
    form.form({
        inline: true,
        on: 'blur',
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter your full name'
                    }
                ]
            },
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'email',
                        prompt: 'Please enter a valid e-mail address'
                    }
                ]
            },
            emailConfirmation: {
                identifier: 'emailConfirmation',
                rules: [
                    {
                        type: 'match[email]',
                        prompt: 'E-mail does not match'
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [
                    {
                        type: 'minLength[8]',
                        prompt: 'Please enter a password with at least 8 characters'
                    }
                ]
            },
            passwordConfirmation: {
                identifier: 'passwordConfirmation',
                rules: [
                    {
                        type: 'match[password]',
                        prompt: 'Password does not match'
                    }
                ]
            },
            terms: {
                identifier: 'terms',
                rules: [
                    {
                        type: 'checked',
                        prompt: 'You must accept the <i>terms and conditions</i> to continue'
                    }
                ]
            }
        }
    });

    function signupSuccess(url) {
        window.swoe.Modal.show('message', {
            approve: {
                callback: function() {
                    window.location.href = url;
                },
                color: 'green'
            },
            closeable: true,
            content: '<p>Your account has been created successfully!</p><p>You will receive an e-mail shortly with a link to confirm your subscription. Please click on that link to finalize the registration process and access the SWoE tool.</p>',
            header: {
                color: 'green',
                icon: 'checkmark',
                text: 'Account created!'
            }
        });
    };

    function signupErrors(errors) {
        window.swoe.Modal.show('message', {
            approve: {
                color: 'yellow'
            },
            closeable: true,
            content: '<p>We could not create your account right now. :(</p><p>' + errors.join('<br />') + '</p>',
            header: {
                color: 'yellow',
                icon: 'warning sign',
                text: 'Account creation failed'
            }
        });
    }

    var dimmer = $('.dimmer');

    form.on('submit', function(evt) {
        evt.preventDefault();
        dimmer.addClass('active');
        $.ajax({
            dataType: 'json',
            method: 'POST',
            cache: false,
            data: form.serialize(),
            url: form.attr('action'),
            success: function(data) {
                dimmer.removeClass('active');
                if(data.status) {
                    signupSuccess(data.data.url);
                } else {
                    signupErrors(data.errors);
                }
            },
            error: function(data) {
                dimmer.removeClass('active');
                signupErrors(data.errors);
            }
        });
    });
})(jQuery);
