/**
 * Created by Gerard on 11/08/2015.
 */
$(function () {
    var form = $('.ui.form');
    form.form({
        inline: true,
        on: 'blur',
        fields: {
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'email',
                        prompt: 'Please enter your e-mail address'
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter your password'
                    }
                ]
            }
        }
    });

    $('.ui.checkbox').checkbox();

    function loginErrors(errors) {
        window.swoe.Modal.show('message', {
            approve: {
                color: 'yellow'
            },
            closeable: true,
            content: '<p>We could not log you in right now. :(</p><p>' + errors.join('<br />') + '</p>',
            header: {
                color: 'yellow',
                icon: 'warning sign',
                text: 'Sign in failed'
            }
        });
    }

    form.on('submit', function(evt) {
        evt.preventDefault();

        $.ajax({
            dataType: 'json',
            method: 'POST',
            cache: false,
            data: form.serialize(),
            url: form.attr('action'),
            success: function(data) {
                if(data.status) {
                    window.location.href = data.data.url;
                } else {
                    loginErrors(data.errors);
                }
            },
            error: function(data) {
                loginErrors(data.responseJSON.errors);
            }
        });
    });
});
