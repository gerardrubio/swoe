/**
 * Created by Gerard on 13/08/2015.
 */
(function($) {
    $.fn.dataTableExt.oStdClasses.sPaging = 'ui buttons right floated ';
    $.fn.dataTableExt.oStdClasses.sPageButtonActive = window.swoe.area.color + ' active';
    $.fn.dataTableExt.oStdClasses.sPageButton = 'ui basic button';
    $.fn.dataTableExt.oStdClasses.sFilter = 'ui icon input';

    var _deviceList = $('.device-list');

    var table = _deviceList.DataTable({
        serverSide: true,
        processing: true,
        deferRender: true,
        stateSave: true,
        paging: true,
        lengthChange: false,
        pageLength: 15,
        responsive: true,
        searching: true,
        ordering: false,
        ajax: {
            url: '/admin/devices/list',
            type: 'GET'
        },
        columns: [
            {
                data: '_id.$id'
            },
            {
                data: 'name'
            },
            {
                className: 'ui center aligned',
                data: 'location.coordinates',
                render: function(data, type, full, meta) {
                    return '<div class="ui icon buttons"><a class="ui basic button" href="/admin/maps?device={id}" title="{coordinates}"><i class="world icon"></i></a></div>'.replace('{id}', full['_id']['$id']).replace('{coordinates}', data);
                }
            },
            {
                className: 'ui center aligned',
                data: null,
                render: function(data, type, full, meta) {
                    return '<div class="ui icon buttons"><a class="ui basic button" href="/admin/charts?device={id}" title="{id}"><i class="line chart icon"></i></a></div>'.replace(/{id}/g, full['_id']['$id']);
                }
            },
            {
                className: 'ui center aligned',
                data: 'owner',
                render: function(data, type, full, meta) {
                    return '<div class="ui icon buttons"><a class="ui basic button" href="/admin/users/edit/{owner}" title="{owner}"><i class="user icon"></i></a></div>'.replace(/{owner}/g, data);
                }
            },
            {
                className: 'ui center aligned',
                data: 'status',
                render: function(data, type, full, meta) {
                    var status_class = 'label-info',
                        status_label = '';

                    switch (data) {
                        case 2:
                            status_class = 'yellow';
                            status_label = 'Inactive';
                            break;
                        case 4:
                            status_class = 'green';
                            status_label = 'Last update: ' + full.updated.sec;
                            break;
                        case 8:
                            status_class = 'grey';
                            status_label = 'Deleted';
                            break;
                        default:
                            status_class = 'brown';
                            status_label = 'Unknown';
                            break;
                    }

                    return '<span class="ui mini {status_class} label">{status_label}</span>'.replace('{status_class}', status_class).replace('{status_label}', status_label);
                }
            },
            {
                className: 'ui center aligned',
                data: null,
                render: function(data, type, full, meta) {
                    return '<div class="ui icon buttons"><a class="ui basic ' + window.swoe.area.color + ' button editDevice" href="/admin/devices/edit/{id}"><i class="setting icon"></i></a>&nbsp;<a class="ui basic red button deleteDevice" href="/admin/devices/delete/{id}" data-name="{name}"><i class="trash outline icon"></i></a></div>'.replace(/{id}/g, data['_id']['$id']).replace('{name}', data['name']);
                }
            }
        ]
    });

    table.on('preXhr', function() {
        $('.ui.main.container').addClass('dimmable dimmed');
    }).on('xhr', function() {
        $('.ui.main.container').removeClass('dimmable dimmed');
    });

    var _deleteDeviceSuccess = function() {
        window.swoe.Modal.show('message', {
            approve: {
                color: window.swoe.area.color
            },
            closeable: true,
            content: "<p>The device has been deleted.</p>",
            header: {
                color: window.swoe.area.color,
                icon: 'trash outline',
                text: 'Delete device'
            }
        });
    };

    var _deleteDeviceErrors = function(errors) {
        window.swoe.Modal.show('message', {
            approve: {
                color: window.swoe.area.color
            },
            closeable: true,
            content: '<p>The device could not be deleted :(</p><p>' + errors.join('<br />') + '</p>',
            header: {
                color: 'yellow',
                icon: 'warning sign',
                text: 'Delete device'
            }
        });
    };

    _deviceList.on('click', 'a.deleteDevice', function(evt) {
        evt.preventDefault();

        var actionUrl = '',
            deviceName = '';

        if (evt.target.hasAttribute('data-name')) {
            actionUrl = evt.target.href;
            deviceName = evt.target.attributes['data-name'].value;
        } else {
            actionUrl = evt.target.parentElement.href;
            deviceName = evt.target.parentElement.attributes['data-name'].value;
        }

        window.swoe.Modal.show('confirm', {
            approve: {
                callback: function () {
                    $.ajax({
                        dataType: 'json',
                        method: 'DELETE',
                        url: actionUrl,
                        success: function(data) {
                            if(data.status) {
                                table.ajax.reload();
                                _deleteDeviceSuccess();
                            } else {
                                _deleteDeviceErrors(data.errors);
                            }
                        },
                        error: function(data) {
                            _deleteDeviceErrors(data.responseJSON.errors);
                        }
                    });

                    return true;
                }
            },
            closeable: true,
            content: "<p>You're about to delete the device <i>" + deviceName + "</i>. All related readings will be deleted too. Are you sure?</p>",
            header: {
                color: window.swoe.area.color,
                icon: window.swoe.area.icon,
                text: 'Delete device'
            }
        });
    });
})(jQuery);
