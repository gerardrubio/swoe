/**
 * Created by Gerard on 15/08/2015.
 */
(function($, targets) {
    var defaults = {
        center: [0.0, 0.0],
        defaultCenter: [-3.7, 40.433333],
        geocodeZoom: 18,
        minZoom: 2,
        zoom: 6
    };

    var view = new ol.View(defaults);

    var tiles = new ol.layer.Tile({
        source: new ol.source.Stamen({
            layer: 'toner-lite',
            url: '//stamen-tiles.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png'
        })
    });

    var positionFeature = new ol.Feature();
    positionFeature.setStyle(new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
                color: '#3399CC'
            }),
            stroke: new ol.style.Stroke({
                color: '#fff',
                width: 2
            })
        })
    }));

    var accuracyFeature = new ol.Feature();
    var featuresOverlay = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [positionFeature, accuracyFeature]
        })
    });

    var map = new ol.Map({
        target: targets['map'].replace('#', ''),
        controls: [],
        layers: [
            tiles, featuresOverlay
        ],
        view: view
    });

    var geolocation = new ol.Geolocation({
        projection: view.getProjection()
    });

    geolocation.on('change:accuracyGeometry', function() {
        console.log('change:accuracyGeometry');
        accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
    });

    var fromOpenLayersToLongitudeLatitude = function(coords) {
        return ol.proj.transform(coords, 'EPSG:3857', 'EPSG:4326')
    };

    var fromLongitudeLatitudeToOpenLayers = function(coords) {
        return ol.proj.transform(coords, 'EPSG:4326', 'EPSG:3857');
    };

    geolocation.on('change:position', function() {
        geolocation.unbind('change:position');
        geolocation.setTracking(false);

        var coordinates = geolocation.getPosition();
        var lonlat = fromOpenLayersToLongitudeLatitude(coordinates);

        updateCoordinates(lonlat[0], lonlat[1]);
        reverseGeocode({
            'lat': lonlat[1],
            'lng': lonlat[0]
        });
    });

    var centerMap = function(latitude, longitude, zoom) {
        var coordinates = fromLongitudeLatitudeToOpenLayers([latitude, longitude]);
        var point = coordinates ? new ol.geom.Point(coordinates) : null;
        zoom = zoom || defaults.zoom;
        positionFeature.setGeometry(point);
        view.setCenter(coordinates);
        view.setZoom(zoom);
    };

    geolocation.on('error', function(error) {
        geolocation.unbind('change:position');
        geolocation.setTracking(false);

        var userDeniedMessage = 'User denied geolocation prompt';
        if (error.message === userDeniedMessage) {
            centerMap(defaults.defaultCenter[0], defaults.defaultCenter[1]);
        }
    });

    var geocoder = new google.maps.Geocoder();
    var geocodeAddress = function(address) {
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                centerAndUpdate(results[0].geometry.location.K, results[0].geometry.location.G);
            }
        });
    };

    var reverseGeocode = function(coordinates) {
        geocoder.geocode({
            'location': coordinates
        }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    centerMap(coordinates.lng, coordinates.lat, defaults.geocodeZoom);
                    $(targets.address).val(results[0].formatted_address);
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
                centerMap(coordinates.lng, coordinates.lat, defaults.geocodeZoom);
            }
        });
    };

    var updateCoordinates = function(latitude, longitude) {
        $(targets.coordinates).val(longitude + ', ' + latitude);
    };

    var centerAndUpdate = function(latitude, longitude) {
        centerMap(latitude, longitude, defaults.geocodeZoom);
        updateCoordinates(latitude, longitude);
    };

    $(targets.geocode).on('click', function(evt) {
        evt.preventDefault();

        var address = $(targets.address).val().trim();
        if (address.length > 0)
            geocodeAddress(address);
    });

    $(targets.geolocate).on('click', function(evt) {
        evt.preventDefault();

        geolocation.setTracking(true);
    });

    var reverseGeocodeFromString = function(coordinates) {
        var latlngStr = coordinates.split(',', 2);
        var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
        reverseGeocode(latlng);
    };

    $(targets.reversegeocode).on('click', function(evt) {
        evt.preventDefault();

        reverseGeocodeFromString($(targets.coordinates).val());
    });

    if ($(targets.owner).attr('readonly')) {
        $(targets.owner).dropdown();
    } else {
        $(targets.owner).dropdown({
            apiSettings: {
                url: '/admin/users/search?q={query}',
                onResponse: function (rawResponse) {
                    var response = {
                        'results': [],
                        'success': rawResponse.status
                    };

                    for (var i in rawResponse.data) {
                        response.results.push({
                            value: rawResponse.data[i]['_id']['$id'],
                            name: rawResponse.data[i]['name'] + ' ' + rawResponse.data[i]['surname'] + ' (' + rawResponse.data[i]['company'] + ')'
                        });
                    }

                    return response;
                }
            }
        });
    }

    $(targets.protocol).dropdown({
        apiSettings: {
            url: '/admin/protocols/search?q={query}',
            onResponse: function(rawResponse) {
                var response = {
                    'results': [],
                    'success': rawResponse.status
                };

                for (var i in rawResponse.data) {
                    response.results.push({
                        value: rawResponse.data[i]['_id']['$id'],
                        name: rawResponse.data[i]['name']
                    });
                }

                return response;
            }
        }
    });

    function saveDeviceSuccess() {
        window.swoe.Modal.show('message', {
            approve: {
                callback: function () {
                    window.location.href = '/admin/devices';
                    return true;
                },
                color: window.swoe.area.color
            },
            closeable: true,
            content: "<p>Your device was created properly! :)</p>",
            header: {
                color: window.swoe.area.color,
                icon: 'checkmark',
                text: 'Create device'
            }
        });
    }

    function saveDeviceErrors(errors) {
        window.swoe.Modal.show('message', {
            approve: {
                color: 'red'
            },
            closeable: true,
            content: '<p>The device could not be created :(</p><p>' + errors.join('<br />') + '</p>',
            header: {
                color: 'yellow',
                icon: 'warning sign',
                text: 'Create device'
            }
        });
    }

    $(document).on('submit', targets.save + ',' + targets.form, function(evt) {
        evt.preventDefault();

        $.ajax({
            dataType: 'json',
            method: 'POST',
            cache: false,
            data: $(targets.form).serialize(),
            url: $(targets.form).attr('action'),
            success: function(data) {
                if(data.status) {
                    saveDeviceSuccess();
                } else {
                    saveDeviceErrors(data.errors);
                }
            },
            error: function(data) {
                saveDeviceErrors(data.responseJSON.errors);
            }
        });
    });

    $(targets.cancel).on('click', function(evt) {
        evt.preventDefault();

        window.swoe.Modal.show('confirm', {
            approve: {
                callback: function () {
                    window.location.href = '/admin/devices';
                    return true;
                }
            },
            closeable: false,
            content: "<p>All changes done will be lost. Are you sure?</p>",
            header: {
                color: window.swoe.area.color,
                icon: window.swoe.area.icon,
                text: 'Discard changes?'
            }
        });
    });

    $('.ui.form').form({
        inline : true,
        on     : 'blur',
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter your name'
                    }
                ]
            },
            owner: {
                identifier: 'owner',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please assign this device to an existing user'
                    }
                ]
            },
            address: {
                identifier: 'address',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter an address'
                    }
                ]
            },
            coordinates: {
                identifier: 'coordinates',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter valid coordinates'
                    }
                ]
            },
            protocol: {
                identifier: 'protocol',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please select a protocol'
                    }
                ]
            },
            ip: {
                identifier: 'ip',
                rules: [
                    {
                        type: 'regExp[/^(^(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.){3}(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))$)|(^([a-f0-9]{1,4}\:){7}[a-f0-9]{1,4})$/]',
                        prompt: 'Please enter a valid IP address (v4 or v6)'
                    }
                ]
            }
        }
    });

    var currentCoordinates = $(targets.coordinates).val();
    if (currentCoordinates.length > 0) {
        reverseGeocodeFromString(currentCoordinates);
    }

})(jQuery, {
    form: '#newDeviceForm',
    save: '.save-device',
    cancel: '.cancel-device',
    address: 'input#address',
    coordinates: 'input#coordinates',
    map: '#map',
    geolocate: '.geolocate',
    geocode: '.geocode',
    reversegeocode: '.reversegeocode',
    owner: 'select#owner',
    protocol: 'select#protocol'
});
