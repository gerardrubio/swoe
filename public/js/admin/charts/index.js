/**
 * Created by Gerard on 18/08/2015.
 */
(function(S, $, targets) {

    var _chartDimmer = $(targets.chartcontainer);

    $('#device.ui.dropdown').dropdown({
        apiSettings: {
            url: '/admin/devices/search?q={query}',
            onResponse: function(rawResponse) {
                var response = {
                    'results': [],
                    'success': rawResponse.status
                };

                for (var i in rawResponse.data) {
                    response.results.push({
                        value: rawResponse.data[i]['_id']['$id'],
                        name: rawResponse.data[i]['name']
                    });
                }

                return response;
            }
        },
        delay: {
            search: 150
        },
        match: 'text',
        placeholder: 'Search for a device',
        sortSelect: true
    });

    $('#dimension.ui.dropdown').dropdown({
        apiSettings: {
            url: '/admin/dimensions/search?q={query}',
            onResponse: function(rawResponse) {
                var response = {
                    'results': [],
                    'success': rawResponse.status
                };

                for (var i in rawResponse.data) {
                    response.results.push({
                        value: rawResponse.data[i]['_id']['$id'],
                        name: rawResponse.data[i]['name'] + ' (' + rawResponse.data[i]['units'] + ')'
                    });
                }

                return response;
            }
        },
        delay: {
            search: 150
        },
        match: 'text',
        placeholder: 'Search for a magnitude to display',
        sortSelect: true
    });

    var loadFilteredData = function(device, dimension, from, until) {
        _chartDimmer.dimmer('show');
        $.ajax({
            dataType: 'json',
            method: 'GET',
            url: '/admin/charts/readings/' + device + '/' + dimension + '/' + from + '/' + until,
            success: function (data) {
                if (data.status && data.data.length > 0)
                    loadChart(data.data);
                else {
                    S.Modal.show('message', {
                        approve: {
                            color: S.area.color
                        },
                        content: '<p>There are no readings available for the combination selected.</p><p>Please try changing the date period or the magnitude to display.</p>',
                        header: {
                            color: S.area.color,
                            icon: S.area.icon,
                            text: 'Data not found!'
                        }
                    });
                }

                _chartDimmer.dimmer('hide');
            },
            error: function (err) {
                console.log(err);
                _chartDimmer.dimmer('hide');
            }
        });
    };

    var convertDataToFlot = function(data) {
        var converted = [];
        for(var d in data) {
            converted.push([data[d].timestamp * 1000, data[d].value]);
        }

        return converted;
    };

    var loadChart = function(data) {
        var options = {
            xaxis: {
                mode: 'time',
                timeformat: '%Y/%m/%d'
            },
            grid: {
                hoverable: true
            },
            curvedLines: {
                active: true,
                apply: true
            },
            series: {
                autoMarkings: {
                    enabled: true,
                    showMinMax: true,
                    showAvg: true
                }
            },
            tooltip: {
                show: true,
                content: '%x - %y.2'
            }
        };
        $.plot(targets.chart, [convertDataToFlot(data)], options);
    };

    $(targets.from + ',' + targets.until).pickadate({
        formatSubmit: 'yyyy-mm-dd',
        hiddenName: true
    });

    $(targets.filterChart).on('submit', function(evt) {
        evt.preventDefault();
        var deviceId = $(targets.device).val(),
            dimensionId = $(targets.dimension).val();

        if (deviceId == null || dimensionId == null)
            return;

        loadFilteredData(
            deviceId,
            dimensionId,
            $('input[name="' + targets.from.replace('#','') + '"]').val(),
            $('input[name="' + targets.until.replace('#','') + '"]').val()
        );
    });

    $(targets.filterChart).submit();
})(window.swoe, jQuery, {
    dimension: '#dimension',
    device: '#device',
    from: '#from',
    until: '#until',
    chart: '.readingschart',
    chartcontainer: '.chartcontainer',
    filterChart: '.filter-chart'
});
