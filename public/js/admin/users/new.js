/**
 * Created by Gerard on 23/08/2015.
 */
(function($, targets) {
    $('.ui.dropdown').dropdown();

    function saveUserSuccess() {
        window.swoe.Modal.show('message', {
            approve: {
                callback: function () {
                    window.location.href = '/admin/users';
                    return true;
                },
                color: window.swoe.area.color
            },
            closeable: true,
            content: "<p>Your user was created properly! :)</p>",
            header: {
                color: window.swoe.area.color,
                icon: 'checkmark',
                text: 'Create user'
            }
        });
    }

    function saveUserErrors(errors) {
        window.swoe.Modal.show('message', {
            approve: {
                color: 'red'
            },
            closeable: true,
            content: '<p>The user could not be created :(</p><p>' + errors.join('<br />') + '</p>',
            header: {
                color: 'yellow',
                icon: 'warning sign',
                text: 'Create user'
            }
        });
    }

    $(document).on('submit', targets.save + ',' + targets.form, function(evt) {
        evt.preventDefault();

        $.ajax({
            dataType: 'json',
            method: 'POST',
            cache: false,
            data: $(targets.form).serialize(),
            url: $(targets.form).attr('action'),
            success: function(data) {
                if(data.status) {
                    saveUserSuccess();
                } else {
                    saveUserErrors(data.errors);
                }
            },
            error: function(data) {
                saveUserErrors(data.responseJSON.errors);
            }
        });
    });

    $(targets.cancel).on('click', function(evt) {
        evt.preventDefault();

        window.swoe.Modal.show('confirm', {
            approve: {
                callback: function () {
                    window.location.href = '/admin/users';
                    return true;
                }
            },
            closeable: false,
            content: "<p>All changes done will be lost. Are you sure?</p>",
            header: {
                color: window.swoe.area.color,
                icon: window.swoe.area.icon,
                text: 'Discard changes?'
            }
        });
    });

    $('.ui.form').form({
        inline : true,
        on     : 'blur',
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter a name'
                    }
                ]
            },
            owner: {
                identifier: 'surname',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter a surname'
                    }
                ]
            },
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'email',
                        prompt: 'Please enter a valid email address'
                    }
                ]
            },
            emailConfirmation: {
                identifier: 'emailConfirmation',
                rules: [
                    {
                        type: 'match[email]',
                        prompt: 'Please repeat the email address'
                    }
                ]
            },
            password: {
                identifier: 'password',
                rules: [
                    {
                        type: 'minLength[8]',
                        prompt: 'Please enter a password with at least 8 characters'
                    }
                ]
            },
            passwordConfirmation: {
                identifier: 'passwordConfirmation',
                rules: [
                    {
                        type: 'match[password]',
                        prompt: 'Please repeat the password'
                    }
                ]
            },
            role: {
                identifier: 'role',
                optional: true,
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please select a role'
                    }
                ]
            }
        }
    });

})(jQuery, {
    form: '#newUserForm',
    save: '.save-user',
    cancel: '.cancel-user'
});
