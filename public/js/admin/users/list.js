/**
 * Created by Gerard on 13/08/2015.
 */
(function($) {
    $.fn.dataTableExt.oStdClasses.sPaging = 'ui buttons right floated ';
    $.fn.dataTableExt.oStdClasses.sPageButtonActive = window.swoe.area.color + ' active';
    $.fn.dataTableExt.oStdClasses.sPageButton = 'ui basic button';
    $.fn.dataTableExt.oStdClasses.sFilter = 'ui icon input';

    var _userList = $('.user-list');

    var table = _userList.DataTable({
        serverSide: true,
        processing: true,
        deferRender: true,
        stateSave: true,
        paging: true,
        lengthChange: false,
        pageLength: 15,
        responsive: true,
        searching: true,
        ordering: false,
        ajax: {
            url: '/admin/users/list',
            type: 'GET'
        },
        columns: [
            {
                data: '_id.$id'
            },
            {
                data: 'name'
            },
            {
                data: 'surname'
            },
            {
                data: 'email'
            },
            {
                className: 'ui center aligned',
                data: 'role',
                render: function(data, type, full, meta) {
                    var status_class = 'label-info',
                        status_label = '';

                    switch (parseInt(data, 10)) {
                        case 2:
                            status_class = 'green';
                            status_label = 'Registered user';
                            break;
                        case 4:
                            status_class = 'red';
                            status_label = 'Administrator';
                            break;
                        default:
                            status_class = 'brown';
                            status_label = 'Unknown';
                            break;
                    }

                    return '<span class="ui mini {status_class} label">{status_label}</span>'.replace('{status_class}', status_class).replace('{status_label}', status_label);
                }
            },
            {
                className: 'ui center aligned',
                data: 'status',
                render: function(data, type, full, meta) {
                    var status_class = 'label-info',
                        status_label = '';

                    switch (data) {
                        case 2:
                            status_class = 'yellow';
                            status_label = 'Inactive';
                            break;
                        case 4:
                            status_class = 'green';
                            status_label = 'Last update: ' + new Date(full.updated.sec * 1000).toDateString();
                            break;
                        case 8:
                            status_class = 'grey';
                            status_label = 'Deleted';
                            break;
                        default:
                            status_class = 'brown';
                            status_label = 'Unknown';
                            break;
                    }

                    return '<span class="ui mini {status_class} label">{status_label}</span>'.replace('{status_class}', status_class).replace('{status_label}', status_label);
                }
            },
            {
                className: 'ui center aligned',
                data: null,
                render: function(data, type, full, meta) {
                    return '<div class="ui icon buttons"><a class="ui basic ' + window.swoe.area.color + ' button editUser" href="/admin/users/edit/{id}"><i class="setting icon"></i></a>&nbsp;<a class="ui basic red button deleteUser" href="/admin/users/delete/{id}" data-name="{name}" data-email="{email}"><i class="trash outline icon"></i></a></div>'.replace(/{id}/g, data['_id']['$id']).replace('{name}', data['name'] + ' ' + data['surname']).replace('{email}', data['email']);
                }
            }
        ]
    });

    table.on('preXhr', function() {
        $('.ui.main.container').addClass('dimmable dimmed');
    }).on('xhr', function() {
        $('.ui.main.container').removeClass('dimmable dimmed');
    });

    function deleteUserSuccess() {
        window.swoe.Modal.show('message', {
            approve: {
                color: window.swoe.area.color
            },
            closeable: true,
            content: "<p>The user has been deleted.</p>",
            header: {
                color: window.swoe.area.color,
                icon: 'trash outline',
                text: 'Delete user'
            }
        });
    };

    function deleteUserErrors(errors) {
        window.swoe.Modal.show('message', {
            approve: {
                color: window.swoe.area.color
            },
            closeable: true,
            content: '<p>The user could not be deleted :(</p><p>' + errors.join('<br />') + '</p>',
            header: {
                color: 'yellow',
                icon: 'warning sign',
                text: 'Delete user'
            }
        });
    };

    _userList.on('click', 'a.deleteUser', function(evt) {
        evt.preventDefault();

        var actionUrl = '',
            userEmail = '',
            userName = '';

        if (evt.target.hasAttribute('data-name')) {
            actionUrl = evt.target.href;
            userEmail = evt.target.attributes['data-email'].value;
            userName = evt.target.attributes['data-name'].value;
        } else {
            actionUrl = evt.target.parentElement.href;
            userEmail = evt.target.parentElement.attributes['data-email'].value;
            userName = evt.target.parentElement.attributes['data-name'].value;
        }

        window.swoe.Modal.show('confirm', {
            approve: {
                callback: function () {
                    $.ajax({
                        dataType: 'json',
                        method: 'DELETE',
                        url: actionUrl,
                        success: function(data) {
                            if(data.status) {
                                table.ajax.reload();
                                deleteUserSuccess();
                            } else {
                                deleteUserErrors(data.errors);
                            }
                        },
                        error: function(data) {
                            deleteUserErrors(data.responseJSON.errors);
                        }
                    });

                    return true;
                }
            },
            closeable: true,
            content: "<p>You're about to delete the user <i>" + userName + " (" + userEmail + ")</i>. All related devices and readings will be deleted too. Are you sure?</p>",
            header: {
                color: window.swoe.area.color,
                icon: window.swoe.area.icon,
                text: 'Delete user'
            }
        });
    });
})(jQuery);
