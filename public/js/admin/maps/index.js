/**
 * Created by Gerard on 03/08/2015.
 */

(function(S, $, targets) {
    var view = new ol.View({
        center: [0, 0],
        zoom: 8,
        minZoom: 2
    });

    var tiles = new ol.layer.Tile({
        source: new ol.source.Stamen({
            layer: 'toner-lite',
            url: '//stamen-tiles.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png'
        })
    });

    var positionFeature = new ol.Feature();
    positionFeature.setStyle(new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
                color: '#3399CC'
            }),
            stroke: new ol.style.Stroke({
                color: '#fff',
                width: 2
            })
        })
    }));

    var deviceSource = new ol.source.Vector({
        source: []
    });

    var clusterSource = new ol.source.Cluster({
        distance: 40,
        source: deviceSource
    });

    var styleCache = {};
    var deviceOverlay = new ol.layer.Vector({
        source: clusterSource,
        style: function(feature, resolution) {
            var size = feature.get('features').length;
            var style = styleCache[size];
            if (!style) {
                if (size > 1) {
                    style = [new ol.style.Style({
                        text: new ol.style.Text({
                            text: '\uf140',
                            font: 'normal 18px Icons',
                            textBaseline: 'Bottom',
                            fill: new ol.style.Fill({
                                color: S.area.color
                            }),
                            stroke: new ol.style.Stroke({
                                color: '#fff',
                                width: 3
                            })
                        })
                    })];
                } else {
                    style = [new ol.style.Style({
                        text: new ol.style.Text({
                            text: '\uf0e7',
                            font: 'normal 18px Icons',
                            textBaseline: 'Bottom',
                            fill: new ol.style.Fill({
                                color: '#ff0'
                            }),
                            stroke: new ol.style.Stroke({
                                color: '#000',
                                width: 3
                            })
                        })
                    })];
                }
                styleCache[size] = style;
            }
            return style;
        }
    });

    var featuresOverlay = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [positionFeature]
        })
    });

    var map = new ol.Map({
        target: 'main-map',
        controls: [],
        layers: [
            tiles,
            deviceOverlay
        ],
        view: view
    });

    var displayDeviceDetails = function(deviceId) {
        console.log('Device selected! ' + deviceId);
        $.ajax({
            dataType: 'json',
            method: 'GET',
            url: '/admin/devices/info/' + deviceId,
            success: function(data) {
                if (data && data.status) {
                    S.Modal.show('confirm', {
                        approve: {
                            color: S.area.color,
                            text: 'Close'
                        },
                        content: function() {
                            var output = '<p><i class="plug icon"></i> {device.name}</p><p><i class="info icon"></i> {device.description}</p><i class="user icon"></i> {user.name} {user.surname}</p>';

                            return output
                                .replace('{device.name}', data.data.device.name)
                                .replace('{device.description}', data.data.device.description)
                                .replace('{user.name}', data.data.owner.name)
                                .replace('{user.surname}',  data.data.owner.surname);
                        },
                        deny: {
                            callback: function() {
                                window.location.href = '/admin/devices/edit/' + deviceId
                            },
                            color: 'olive',
                            icon: 'info',
                            text: 'View'
                        },
                        header: {
                            color: 'olive',
                            icon: 'plug',
                            text: 'Device info'
                        }
                    });
                }
            }
        });
    };

    map.getViewport().addEventListener('click', function (e) {
        e.preventDefault();

        var feature = map.forEachFeatureAtPixel(map.getEventPixel(e),
            function (feature, layer) {
                return feature;
            }
        );

        if (feature && feature.values_ && feature.values_.features && feature.values_.features.length == 1) {
            displayDeviceDetails(feature.values_.features[0].id_);
        }
    });

    var geolocation = new ol.Geolocation({
        projection: view.getProjection()
    });

    var fromOpenLayersToLongitudeLatitude = function(coords) {
        return ol.proj.transform(coords, 'EPSG:3857', 'EPSG:4326')
    };

    var fromLongitudeLatitudeToOpenLayers = function(coords) {
        return ol.proj.transform(coords, 'EPSG:4326', 'EPSG:3857');
    };

    geolocation.on('change:position', function() {
        geolocation.unbind('change:position');
        geolocation.setTracking(false);

        map.addLayer(featuresOverlay);

        var coordinates = geolocation.getPosition();
        var lonlat = fromOpenLayersToLongitudeLatitude(coordinates);

        var point = coordinates ? new ol.geom.Point(coordinates) : null;
        positionFeature.setGeometry(point);
        centerMap(lonlat[0], lonlat[1]);
        //updateMap();
    });

    var centerMap = function(latitude, longitude) {
        var coordinates = fromLongitudeLatitudeToOpenLayers([latitude, longitude]);

        view.setCenter(coordinates);
    };

    var searchData = {
        deviceId: $(targets.device).val(),
        ownerId: $(targets.owner).val()
    };

    var updateMap = function() {
        $.ajax({
            cache: false,
            method: 'GET',
            url: '/admin/maps/locate/' + searchData.ownerId + '/' + searchData.deviceId,
            success: function(data) {
                data = JSON.parse(data);
                if (data && data.status) {
                    deviceSource.clear();

                    var features = [], feature, geom;
                    var devices = data.data.devices.length;
                    if (devices > 0) {
                        for (var i = 0; i < devices; i++) {
                            var device = data.data.devices[i];
                            geom = new ol.geom.Point(
                                fromLongitudeLatitudeToOpenLayers([device.location.coordinates[1], device.location.coordinates[0]])
                            );

                            feature = new ol.Feature(geom);
                            feature.setId(device['_id']['$id']);
                            features.push(feature);
                        }

                        if (devices === 1) {
                            centerMap(data.data.devices[0].location.coordinates[1], data.data.devices[0].location.coordinates[0]);
                        }

                        deviceSource.addFeatures(features);
                        console.log(devices + ' devices added!');
                    } else {
                        S.Modal.show('message', {
                            approve: {
                                color: S.area.color
                            },
                            content: '<p>There are no devices available for the combination selected.</p><p>Please try changing owner or the device selected.</p>',
                            header: {
                                color: S.area.color,
                                icon: S.area.icon,
                                text: 'Data not found!'
                            }
                        });
                    }
                }
            }
        });
    };

    geolocation.on('error', function(error) {
        geolocation.unbind('change:position');
        geolocation.setTracking(false);

        var userDeniedMessage = 'User denied geolocation prompt';
        if (error.message === userDeniedMessage) {
            centerMap(-3.7, 40.433333);
            updateMap(-3.7, 40.433333);
        }
    });

    geolocation.setTracking(true);

    $(targets.device).dropdown({
        apiSettings: {
            url: '/admin/devices/search?q={query}&owner=' + searchData.ownerId,
            onChange: function(value, text, $choice) {
                searchData.deviceId = value;
            },
            onResponse: function(rawResponse) {
                var response = {
                    'results': [],
                    'success': rawResponse.status
                };

                for (var i in rawResponse.data) {
                    response.results.push({
                        value: rawResponse.data[i]['_id']['$id'],
                        name: rawResponse.data[i]['name']
                    });
                }

                return response;
            }
        },
        delay: {
            search: 150
        },
        match: 'text',
        placeholder: 'Search for a device',
        sortSelect: true
    });

    $(targets.owner).dropdown({
        apiSettings: {
            url: '/admin/users/search?q={query}',
            onChange: function(value, text, $choice) {
                searchData.ownerId = value;
            },
            onResponse: function(rawResponse) {
                var response = {
                    'results': [],
                    'success': rawResponse.status
                };

                for (var i in rawResponse.data) {
                    response.results.push({
                        value: rawResponse.data[i]['_id']['$id'],
                        name: '{name} {surname} ({company})'
                            .replace('{name}', rawResponse.data[i]['name'])
                            .replace('{surname}', rawResponse.data[i]['surname'])
                            .replace('{company}', rawResponse.data[i]['company'])
                    });
                }

                $(targets.device).dropdown('set selected', '', '');
                return response;
            }
        },
        delay: {
            search: 150
        },
        match: 'text',
        placeholder: 'Search for a user',
        sortSelect: true
    });

    $(targets.filter).on('submit', function(evt) {
        evt.preventDefault();

        var deviceId = $(targets.device).val(),
            ownerId = $(targets.owner).val();

        searchData = {
            deviceId: deviceId,
            ownerId: ownerId
        };

        console.log('filter by ' + ownerId + ' and ' + deviceId);
        updateMap();
    });

    $(targets.filter).submit();
})(window.swoe, jQuery, {
    device: '#device',
    filter: '.filter-map',
    owner: '#owner'
});

