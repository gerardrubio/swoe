/**
 * Created by Gerard on 03/08/2015.
 */

(function(S, $, targets) {
    var defaults = {
        center: [0.0, 0.0],
        defaultCenter: [-3.7, 40.433333],
        geocodeZoom: 18,
        minZoom: 2,
        zoom: 14
    };

    var view = new ol.View(defaults);

    var tiles = new ol.layer.Tile({
        source: new ol.source.Stamen({
            layer: 'toner-lite',
            url: '//stamen-tiles.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png'
        })
    });

    var positionFeature = new ol.Feature();
    positionFeature.setStyle(new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
                color: '#3399CC'
            }),
            stroke: new ol.style.Stroke({
                color: '#fff',
                width: 2
            })
        })
    }));

    var deviceSource = new ol.source.Vector({
        source: []
    });

    var clusterSource = new ol.source.Cluster({
        distance: 40,
        source: deviceSource
    });

    var styleCache = {};
    var deviceOverlay = new ol.layer.Vector({
        source: clusterSource,
        style: function(feature, resolution) {
            var size = feature.get('features').length;
            var style = styleCache[size];
            if (!style) {
                if (size > 1) {
                    style = [new ol.style.Style({
                        text: new ol.style.Text({
                            text: '\uf140',
                            font: 'normal 18px Icons',
                            textBaseline: 'Bottom',
                            fill: new ol.style.Fill({
                                color: S.area.color
                            }),
                            stroke: new ol.style.Stroke({
                                color: '#fff',
                                width: 3
                            })
                        })
                    })];
                } else {
                    style = [new ol.style.Style({
                        text: new ol.style.Text({
                            text: '\uf0e7',
                            font: 'normal 18px Icons',
                            textBaseline: 'Bottom',
                            fill: new ol.style.Fill({
                                color: '#ff0'
                            }),
                            stroke: new ol.style.Stroke({
                                color: '#000',
                                width: 3
                            })
                        })
                    })];
                }
                styleCache[size] = style;
            }
            return style;
        }
    });

    var featuresOverlay = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [positionFeature]
        })
    });

    var map = new ol.Map({
        target: 'main-map',
        controls: [],
        layers: [
            tiles,
            deviceOverlay
        ],
        view: view
    });

    var displayDeviceDetails = function(deviceId) {
        console.log('Device selected! ' + deviceId);
        $.ajax({
            dataType: 'json',
            method: 'GET',
            url: '/admin/devices/info/' + deviceId,
            success: function(data) {
                if (data && data.status) {
                    S.Modal.show('confirm', {
                        approve: {
                            color: S.area.color,
                            text: 'Close'
                        },
                        content: function() {
                            var output = '<p><i class="plug icon"></i> {device.name}</p><p><i class="info icon"></i> {device.description}</p><i class="user icon"></i> {user.name} {user.surname}</p>';

                            return output
                                .replace('{device.name}', data.data.device.name)
                                .replace('{device.description}', data.data.device.description)
                                .replace('{user.name}', data.data.owner.name)
                                .replace('{user.surname}',  data.data.owner.surname);
                        },
                        deny: {
                            callback: function() {
                                window.location.href = '/admin/devices/edit/' + deviceId
                            },
                            color: 'olive',
                            icon: 'info',
                            text: 'View'
                        },
                        header: {
                            color: 'olive',
                            icon: 'plug',
                            text: 'Device info'
                        }
                    });
                }
            }
        });
    };

    map.getViewport().addEventListener('click', function (e) {
        e.preventDefault();

        var feature = map.forEachFeatureAtPixel(map.getEventPixel(e),
            function (feature, layer) {
                return feature;
            }
        );

        if (feature && feature.values_ && feature.values_.features && feature.values_.features.length == 1) {
            displayDeviceDetails(feature.values_.features[0].id_);
        }
    });

    var geolocation = new ol.Geolocation({
        projection: view.getProjection()
    });

    var fromOpenLayersToLongitudeLatitude = function(coords) {
        return ol.proj.transform(coords, 'EPSG:3857', 'EPSG:4326')
    };

    var fromLongitudeLatitudeToOpenLayers = function(coords) {
        return ol.proj.transform(coords, 'EPSG:4326', 'EPSG:3857');
    };

    geolocation.on('change:position', function() {
        geolocation.unbind('change:position');
        geolocation.setTracking(false);

        map.addLayer(featuresOverlay);

        var coordinates = geolocation.getPosition();
        var lonlat = fromOpenLayersToLongitudeLatitude(coordinates);

        updateCoordinates(lonlat[0], lonlat[1]);
        reverseGeocode({
            'lat': lonlat[1],
            'lng': lonlat[0]
        });
        moveUserPosition(lonlat[0], lonlat[1]);
    });

    var centerMap = function(latitude, longitude) {
        var coordinates = fromLongitudeLatitudeToOpenLayers([latitude, longitude]);

        view.setCenter(coordinates);
    };

    var moveUserPosition = function(latitude, longitude) {
        var coordinates = fromLongitudeLatitudeToOpenLayers([latitude, longitude]);
        var point = coordinates ? new ol.geom.Point(coordinates) : null;
        positionFeature.setGeometry(point);
    };

    var searchData = {
        latitude:0.0,
        longitude: 0.0,
        radius: 1000
    };

    var updateMap = function() {
        $.ajax({
            cache: false,
            dataType: 'json',
            method: 'GET',
            url: '/admin/maps/reveal/' + searchData.latitude + '/' + searchData.longitude + '/' + searchData.radius,
            success: function(data) {
                if (data && data.status) {
                    deviceSource.clear();

                    var features = [], feature, geom;
                    var devices = data.data.devices.length;
                    if (devices > 0) {
                        for (var i = 0; i < devices; i++) {
                            var device = data.data.devices[i];
                            geom = new ol.geom.Point(
                                fromLongitudeLatitudeToOpenLayers([device.location.coordinates[1], device.location.coordinates[0]])
                            );

                            feature = new ol.Feature(geom);
                            feature.setId(device['_id']['$id']);
                            features.push(feature);
                        }

                        if (devices === 1) {
                            centerMap(data.data.devices[0].location.coordinates[1], data.data.devices[0].location.coordinates[0]);
                        }

                        deviceSource.addFeatures(features);
                        console.log(devices + ' devices added!');
                    } else {
                        S.Modal.show('message', {
                            approve: {
                                color: S.area.color
                            },
                            content: '<p>There are no devices available for the combination selected.</p><p>Please try changing the search radius or the coordinates.</p>',
                            header: {
                                color: S.area.color,
                                icon: S.area.icon,
                                text: 'Data not found!'
                            }
                        });
                    }
                }
            }
        });
    };

    geolocation.on('error', function(error) {
        geolocation.unbind('change:position');
        geolocation.setTracking(false);

        var userDeniedMessage = 'User denied geolocation prompt';
        if (error.message === userDeniedMessage) {
            centerMap(-3.7, 40.433333);
            updateMap(-3.7, 40.433333);
        }
    });

    geolocation.setTracking(true);

    var geocoder = new google.maps.Geocoder();
    var geocodeAddress = function(address) {
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                centerAndUpdate(results[0].geometry.location.K, results[0].geometry.location.G);
                moveUserPosition(results[0].geometry.location.K, results[0].geometry.location.G);
            }
        });
    };

    var reverseGeocode = function(coordinates) {
        geocoder.geocode({
            'location': coordinates
        }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    centerMap(coordinates.lng, coordinates.lat, defaults.geocodeZoom);
                    $(targets.address).val(results[0].formatted_address);
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
                centerMap(coordinates.lng, coordinates.lat, defaults.geocodeZoom);
            }
        });
    };

    var updateCoordinates = function(latitude, longitude) {
        $(targets.coordinates).val(longitude + ', ' + latitude);
    };

    var centerAndUpdate = function(latitude, longitude) {
        centerMap(latitude, longitude, defaults.geocodeZoom);
        moveUserPosition(latitude, longitude);
        updateCoordinates(latitude, longitude);
    };

    $(targets.geocode).on('click', function(evt) {
        evt.preventDefault();

        var address = $(targets.address).val().trim();
        if (address.length > 0)
            geocodeAddress(address);
    });

    $(targets.geolocate).on('click', function(evt) {
        evt.preventDefault();

        geolocation.setTracking(true);
    });

    $(targets.filter).on('submit', function(evt) {
        evt.preventDefault();

        var coordinates = $(targets.coordinates).val(),
            radius = $(targets.radius).val();

        if (coordinates.indexOf(',') < 0)
            return;

        coordinates = coordinates.split(',');

        searchData = {
            latitude: parseFloat(coordinates[0]),
            longitude: parseFloat(coordinates[1]),
            radius: parseInt(radius, 10)
        };

        updateMap();
    });

    $(targets.geolocate).trigger('click');
})(window.swoe, jQuery, {
    address: '#address',
    coordinates: '#coordinates',
    filter: '.filter-map',
    geolocate: '.geolocate',
    geocode: '.geocode',
    reversegeocode: '.reversegeocode',
    radius: '#radius'
});
