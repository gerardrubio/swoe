/**
 * Created by Gerard on 30/08/2015.
 */
(function($) {
    var form = $('.ui.form');
    form.form({
        inline: true,
        on: 'blur',
        fields: {
            name: {
                identifier: 'name',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter your full name'
                    }
                ]
            },
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'email',
                        prompt: 'Please enter a valid e-mail address'
                    }
                ]
            },
            content: {
                identifier: 'content',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter a message'
                    },
                    {
                        type   : 'maxLength[1000]',
                        prompt : 'The message must not exceed 1000 characters'
                    }
                ]
            },
            terms: {
                identifier: 'terms',
                rules: [
                    {
                        type: 'checked',
                        prompt: 'You must accept the <i>terms and conditions</i> to continue'
                    }
                ]
            }
        }
    });

    function contactSuccess() {
        window.swoe.Modal.show('message', {
            approve: {
                callback: function() {
                    window.location.href = '/';
                },
                color: 'green'
            },
            closeable: true,
            content: '<p>Your message has been sent successfully!</p><p>You will receive a copy of the message shortly. Thank you for contacting with us!.</p>',
            header: {
                color: 'green',
                icon: 'mail outline',
                text: 'Message sent!'
            }
        });
    };

    function contactErrors(errors) {
        window.swoe.Modal.show('message', {
            approve: {
                color: 'yellow'
            },
            closeable: true,
            content: '<p>We could not send your message right now. :(</p><p>' + errors.join('<br />') + '</p>',
            header: {
                color: 'yellow',
                icon: 'warning sign',
                text: 'Account creation failed'
            }
        });
    }

    var dimmer = $('.dimmer');

    form.on('submit', function(evt) {
        evt.preventDefault();
        dimmer.addClass('active');
        $.ajax({
            dataType: 'json',
            method: 'POST',
            cache: false,
            data: form.serialize(),
            url: form.attr('action'),
            success: function(data) {
                dimmer.removeClass('active');
                if(data.status) {
                    contactSuccess();
                } else {
                    contactErrors(data.errors);
                }
            },
            error: function(data) {
                dimmer.removeClass('active');
                contactErrors(data.errors);
            }
        });
    });

    $('.ui.checkbox').checkbox();
})(jQuery);
