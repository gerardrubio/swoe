/**
 * Created by Gerard on 03/08/2015.
 */
(function($) {
    var view = new ol.View({
        center: [0, 0],
        zoom: 8,
        minZoom: 2
    });

    var tiles = new ol.layer.Tile({
        source: new ol.source.Stamen({
            layer: 'toner-lite',
            url: '//stamen-tiles.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png'
        })
    });

    var positionFeature = new ol.Feature();
    positionFeature.setStyle(new ol.style.Style({
        image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
                color: '#3399CC'
            }),
            stroke: new ol.style.Stroke({
                color: '#fff',
                width: 2
            })
        })
    }));

    var accuracyFeature = new ol.Feature();
    var featuresOverlay = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [positionFeature, accuracyFeature]
        })
    });

    var heatmapSource = new ol.source.Vector({
        format: new ol.format.GeoJSON()
    });
    var heatmapOverlay = new ol.layer.Heatmap({
        source: heatmapSource,
        blur: 10,
        radius: 5,
        opacity: 0.90
        //gradient: ['#33318B', '#1C75D6', '#1DA0C9', '#46B79B', '#A9BC6E', '#F3C54A', '#F9F739']
        //gradient: ['#1C1D21', '#31353D', '#445878', '#92CDCF', '#EEEFF7']
        //,gradient: ['#6B0C22', '#D9042B', '#F4CB89', '#588C8C', '#011C26']
        ,gradient: ['#FF0000', '#FF6600', '#FFFF00']

        //,gradient: ['#EBEBEB', '#6E6E6E', '#3F3F3F', '#181818', '#BBFE00']//https://color.adobe.com/bolt-color-theme-6374908
        //,gradient: ['#000000', '#333333', '#FF358B', '#01B0F0', '#AEEE00']//https://color.adobe.com/oddend-color-theme-2181
    });

    var map = new ol.Map({
        target: 'header-map',
        controls: [],
        layers: [
            tiles,
            heatmapOverlay,
        ],
        view: view
    });

    var geolocation = new ol.Geolocation({
        projection: view.getProjection()
    });

    geolocation.on('change:accuracyGeometry', function() {
        console.log('change:accuracyGeometry');
        accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
    });

    var fromOpenLayersToLongitudeLatitude = function(coords) {
        return ol.proj.transform(coords, 'EPSG:3857', 'EPSG:4326')
    };

    var fromLongitudeLatitudeToOpenLayers = function(coords) {
        return ol.proj.transform(coords, 'EPSG:4326', 'EPSG:3857');
    };

    geolocation.on('change:position', function() {
        geolocation.unbind('change:position');
        geolocation.setTracking(false);

        map.addLayer(featuresOverlay);

        var coordinates = geolocation.getPosition();
        var lonlat = fromOpenLayersToLongitudeLatitude(coordinates);

        centerMap(lonlat[0], lonlat[1]);
        updateMap(lonlat[0], lonlat[1]);
    });

    var centerMap = function(latitude, longitude) {
        var coordinates = fromLongitudeLatitudeToOpenLayers([latitude, longitude]);
        var point = coordinates ? new ol.geom.Point(coordinates) : null;
        positionFeature.setGeometry(point);
        view.setCenter(coordinates);
    };

    var updateMap = function(latitude, longitude) {
        $.ajax({
            url: '//api.swoe.eu/graph/' + latitude + '/' + longitude,
            success: function(data) {
                data = JSON.parse(data);
                if (data && data.status) {
                    heatmapSource.clear();

                    var features = [], feature, geom;
                    var devices = data.data.devices.length;
                    for (var i = 0; i < devices; i++) {
                        var device = data.data.devices[i];
                        geom = new ol.geom.Point(
                            fromLongitudeLatitudeToOpenLayers([device.location.coordinates[1], device.location.coordinates[0]])
                        );

                        feature = new ol.Feature(geom);
                        feature.set('weight', device.value / data.data.max);
                        features.push(feature);
                    }
                    heatmapSource.addFeatures(features);
                }
            }
        });
    };

    geolocation.on('error', function(error) {
        geolocation.unbind('change:position');
        geolocation.setTracking(false);

        var userDeniedMessage = 'User denied geolocation prompt';
        if (error.message === userDeniedMessage) {
            centerMap(-3.7, 40.433333);
            updateMap(-3.7, 40.433333);
        }
    });

    geolocation.setTracking(true);
})(jQuery);
